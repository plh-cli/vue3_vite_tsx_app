# Vue 3 + TypeScript + Vite

## 安装yarn，安装了就跳过
npm install -g yarn 

## 安装依赖
yarn

> 如果在vscode中执行yarn报禁止运行错误，以管理员身份运行Windows PowerShell，执行
set-ExecutionPolicy RemoteSigned命令，并输入y选项，之后继续执行yarn

## 启动
yarn dev


## 打包
yarn build
